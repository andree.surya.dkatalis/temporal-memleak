import { Connection, Client } from '@temporalio/client';
import { faker } from '@faker-js/faker';
import { processCustomerRegistration } from './workflows';
import { Status } from './customer/customer.interface';

async function run() {
  const connection = await Connection.connect();

  const client = new Client({
    connection,
  });

  const total = 5000;
  for (let i = 0; i < total; i++) {
    console.log(`Starting workflow ${i + 1} / ${total}`);
    const idNumber = faker.string.uuid();

    await client.workflow.start(processCustomerRegistration, {
      args: [
        {
          status: Status.Pending,
          idNumber: idNumber,
          picture: faker.internet.avatar(),
          name: faker.person.fullName(),
          gender: faker.person.gender(),
          email: faker.internet.email(),
          address: faker.location.streetAddress(),
          about: faker.person.bio(),
          tags: faker.lorem.words(20).split(' '),
        },
      ],
      taskQueue: 'default',
      workflowId: `processCustomerRegistration-${idNumber}`,
    });
  }
}

run().catch((err) => {
  console.error(err);
  process.exit(1);
});
