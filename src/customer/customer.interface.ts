export enum Status {
  Active,
  Pending,
  Closed,
}

export interface Customer {
  status: Status;
  gender: string;
  idNumber: string;
  email: string;
  name: string;
  picture: string;
  address: string;
  about: string;
  tags: string[];
}
