import { proxyActivities } from '@temporalio/workflow';
import { Customer } from './customer/customer.interface';
import type * as activities from './activities';

const { registerPendingCustomer } = proxyActivities<typeof activities>({
  startToCloseTimeout: '1 minute',
});

export async function processCustomerRegistration(customer: Customer): Promise<void> {
  await registerPendingCustomer(customer);
}
