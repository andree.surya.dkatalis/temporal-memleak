import { Customer } from './customer/customer.interface';

export async function registerPendingCustomer(customer: Customer): Promise<void> {
  console.log(`Registering customer data ${customer.idNumber}...`);
}
