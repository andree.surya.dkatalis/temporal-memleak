# Temporal Memory Leak Demo

This project demonstrates potential memory leak issues in the SDK. The project contains a mock workflow process for customer registration which simply logs customer ID into the console.

### Running this sample

1. `temporal server start-dev` to start [Temporal Server](https://github.com/temporalio/cli/#installation).
1. `npm install` to install dependencies.
1. `npm run start` to start the Worker.
1. In another shell, `npm run seed` to start seeding mock workflow processes.

In between seeder run, please capture worker's heap snapshots and compare.

### Observation

Between seeder batch runs, we got persistent increase in heap memory usage that doesn't get released even after batch process completion.

| # Workflows | Heap Used |
| ----------- | --------- |
| 0           | 177 MB    |
| 5000        | 319 MB    |
| 10000       | 448 MB    |
| 15000       | 577 MB    |
| 20000       | 707 MB    |
| 25000       | 837 MB    |
